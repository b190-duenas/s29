// SECTION Comparison Query Operators

// $gt/$gte operator
	/*
		- allows us to find documents that have field number values greater than or greater than or equal to a specified number
		SYNTAX:
			db.collectionName.find( { field : { $gt : value } } );
			db.collectionName.find( { field : { $gte : value } } );
	*/
db.users.find({ age : { $gt : 50 } });
db.users.find({ age : { $gte : 50 } });

// $lt/$lte operator
/*
	-allows us to find documents that have field number values less than or less than or equal to a specified number
	SYNTAX:
		db.collectioName.find( { field : { $lt : value } } );
		db.collectioName.find( { field : { $lte : value } } );
*/
db.users.find({ age : { $lt : 50 } });
db.users.find({ age : { $lte : 50 } });

// $ne operator
/*
	- allows us find documents that have field number values that are not equal to a specified number
	SYNTAX
		db.collectionName.find( { field : { $ne : value } } );
*/
db.users.find( { age : { $ne : 82 } } );

// $in operator
/*
	- allows us to find documents with specific match criteria on one field using different values
	SYNTAX:
		db.collectionName.find( field : { $in : value } );
*/
db.users.find({ lastName : { $in : [ "Hawking", "Smith" ] } });
// when looking for a nested array using $in operator
db.users.find({ courses : { $in : [ "HTML", "React" ] } });

// SECTION - Logical Query Operators

// $or operator
/*
	- allows us to find documents matching a single criteria from multiple provided search criteria
	SYNTAX : 
		db.collectionName.find( { $or: [ {fieldA : valueA}, {fieldB : valueB} ] } );
*/
db.users.find( { $or : [ { firstName: "Neil" }, { age : 21 } ] } );
// try to fuse $or operator and gt operator (used in age)
db.users.find( { $or: [ { firstName: "Neil" }, { age: { $gt: 21 } } ] }); // c/o sir gab

// $and operator
/*
	- allows us to find documents matching multiple criteria in a single field
	SYNTAX:
		db.collectionName.find( { $and: [ {fieldA : valueA}, {fieldB : valueB} ] } );
*/
db.users.find( { $and: [ { age: { $ne : 82 } }, { age: { $ne : 76 } } ] } );

// SECTION - Field Projection
/*
	- retrieving documents are common operatuions that we do and by default MongoDB queries return the whole document as a response
	- when dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
	- to help with readability of the values returned, we can include/exclude fields from the response
	- in this section, we are trying to modify the documents in the response of MongoDB, but not the actual document itself when check the collection
*/

// INCLUSION
/*
	- allows us to include specific fields only when retrieving documents
	- the value provided is 1 to denote that the field is being included
	SYNTAX:
		db.collectionName.find( {criteria}, {field : 1} );

	below code is also applicable - c/o sir JC
	db.users.find(
		{ firstName: "Jane" },
		{
			firstName: true,
			lastName: true,
			contact: true
		}
	);
*/
db.users.find(
	{ firstName: "Jane" },
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// EXCLUSION
/*
	- allows us to exclude specific fields only when retrieving documents
	- the value provided is 0 to denote that the field is being excluded
	SYNTAX:
		db.collectionName.find( {criteria}, {field : 0} );

	below code is also applicable - c/o sir JC
	db.users.find(
		{ firstName: "Jane" },
		{
			firstName: false,
			lastName: false,
			contact: false
		}
	);
*/
db.users.find(
	{ firstName: "Jane" },
	{
		contact: 0,
		department: 0
	}
);

/*
	look for a document but do not display the _id of the document, and include the firstName, lastName, and contact

	send the ss of the returned statement in the google chat
*/

// Suppressing of ID field
/*
	- allows us to exclude the "_id" field in the document returned by MongoDB
	- when using field projections, field inclusion and exclusion may be used at the same time
		- this is ONLY APPLICABLE if we are suppressing/excluding the "_id" field
	SYTAX:
		db.collectionName.find( {criteria}, {_id: 0} );
*/
db.users.find(
	{ firstName: "Jane" },
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);

// Returning Specific Fields in Embedded Documents
/*
	what code could solve the problem:
		try to include the following in the returned document
			firstName
			lastName
			only the "phone" in the embedded "contact" document
*/
// one way of coding it - c/o Sir Chano
/*db.users.find(
	{ firstName: "Jane" },
	{
            
		firstName: true,
		lastName: true,
		contact: {
            phone: true
        }
	}
);*/
// another way of coding it
db.users.find(
	{ firstName: "Jane" },
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

// Suppressing Specific Fields in Embedded Documents
// c/o Sir Renz
db.users.find(
	{ firstName: "Jane" },
	{
		"contact.phone": 0
	}
);

// Project Specific Array Elements in the Returned Array

// $slice operator - allows us to retrieve only 1 element that matches the search criteria
db.users.find(
	{
		"nameArr":{nameA: "juan"}
	},
	{
		nameArr:
			{$slice : 1}
	}
);

// SECTION - Evaluation Query Operators
// $regex operator
/*
	- Allows us to find documents that match a specific string pattern using regular expressions
	SYNTAX:
		db.collectionName.find( { field: { $regex: "pattern", $options: "$optionsValue" } } );
*/ 
// Case-sensitive query - retrieve all document with "N" strictly following the casing
db.users.find( { firstName: { $regex: "N" } } );

// Case-insensitive query
db.users.find( { firstName: { $regex: "n", $options: "$i" } } );
// using or, this can also be done through the code below - c/o Sir Renz
/*
db.users.find( { $or: [ { firstName: { $regex: "n"} }, { firstName: { $regex: "N" } } ] } );
*/
